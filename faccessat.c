
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

int main(char **argv, int argc)
{
  int ret;
  errno = 0;
  ret = faccessat(AT_FDCWD, "faccessat.c", R_OK, 0);
  fprintf(stderr, "faccessat: %d (errno %s)\n", ret, strerror(errno));
  return ret == 0 ? 0 : 1;
}
